const express = require('express');
const graphqlHTTP = require('express-graphql');

const schema = require('./schema/schema');
const app = express();

app.use('/grahpql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen('7000', () => {
    console.log('we are listen now');
});
const graphql = require('graphql');
const _ = require('lodash');

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLSchema,
    GraphQLInt
} = graphql;


var books = [{
        name: 'Book 1',
        genre: 'fantasy',
        id: '1'
    },
    {
        name: 'Book 2',
        genre: 'fantasy',
        id: '2'
    },
    {
        name: 'Book 3',
        genre: 'fantasy',
        id: '3'
    }
];


var authors = [{
        name: 'mohamed',
        age: '30',
        id: '1'
    },
    {
        name: 'hend',
        age: '25',
        id: '2'
    },
    {
        name: 'hend',
        age: '40',
        id: '3'
    }
];

const BookType = new GraphQLObjectType({
    name: 'Book',
    fields: () => ({
        id: {
            type: GraphQLID
        },
        name: {
            type: GraphQLString
        },
        genre: {
            type: GraphQLString
        }
    })
});



const AuthorType = new GraphQLObjectType({
    name: 'Author',
    fields: () => ({
        id: {
            type: GraphQLID
        },
        name: {
            type: GraphQLString
        },
        age: {
            type: GraphQLInt
        }
    })
});



const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        book: {
            type: BookType,
            args: {
                id: {
                    type: GraphQLID
                }
            },
            resolve(parent, args) {
                //console.log(typeof (args.id))
                //code get data from db/other source
                return _.find(books, {
                    id: args.id
                });
            }

        },
        author: {
            type: AuthorType,
            args: {
                id: {
                    type: GraphQLID
                }
            },
            resolve(parent, args) {
                //console.log(typeof (args.id))
                //code get data from db/other source
                return _.find(authors, {
                    id: args.id
                });
            }

        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery
});